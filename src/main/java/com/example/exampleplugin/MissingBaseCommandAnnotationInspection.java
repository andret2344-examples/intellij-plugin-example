/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package com.example.exampleplugin;

import com.intellij.codeInspection.*;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class MissingBaseCommandAnnotationInspection extends AbstractBaseJavaLocalInspectionTool {
	@NonNls
	public static final String DESCRIPTION = "Missing the @BaseCommand annotation";

	@NotNull
	@Override
	public PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder, final boolean isOnTheFly) {
		return new JavaElementVisitor() {
			@Override
			public void visitClass(@NotNull final PsiClass aClass) {
				Optional.of(aClass)
						.filter(psiClass -> !(psiClass instanceof PsiTypeParameter))
						.filter(psiClass -> !psiClass.hasAnnotation("eu.andret.arguments.api.annotation.BaseCommand"))
						.map(PsiClass::getSuperClass)
						.map(PsiClass::getQualifiedName)
						.filter("eu.andret.arguments.AnnotatedCommandExecutor"::equals)
						.map(name -> aClass.getNameIdentifier())
						.ifPresent(psiIdentifier -> holder.registerProblem(psiIdentifier, DESCRIPTION,
								ProblemHighlightType.GENERIC_ERROR, getFixes()));
			}

			@NotNull
			private LocalQuickFix[] getFixes() {
				return new LocalQuickFix[]{
						new AddMissingAnnotationQuickFix(),
				};
			}
		};
	}

	public static class AddMissingAnnotationQuickFix implements LocalQuickFix {
		public static final String NAME = "Add @BaseCommand annotation";

		@Override
		public void applyFix(@NotNull final Project project, @NotNull final ProblemDescriptor descriptor) {
			Optional.of(descriptor)
					.map(ProblemDescriptor::getPsiElement)
					.map(PsiElement::getParent)
					.map(PsiClass.class::cast)
					.map(PsiModifierListOwner::getModifierList)
					.ifPresent(modifierList -> {
						final PsiElementFactory factory = JavaPsiFacade.getInstance(project).getElementFactory();
						final PsiAnnotation psiAnnotation = factory.createAnnotationFromText(
								"@eu.andret.arguments.api.annotation.BaseCommand(\"\")",
								modifierList.getParent());
						final PsiElement firstChild = modifierList.getFirstChild();
						final PsiElement inserted = modifierList.addBefore(psiAnnotation, firstChild);
						JavaCodeStyleManager.getInstance(project).shortenClassReferences(inserted);
					});
		}

		@Override
		@NotNull
		public String getFamilyName() {
			return NAME;
		}
	}
}
